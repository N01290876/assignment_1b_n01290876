﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment1b
{
    public partial class WebForm1b : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            TextBox1.Text = DateTime.Today.ToShortDateString() + '.';
        }

        protected void DateChange(object sender, EventArgs e)
        {

            TextBox1.Text = Calendar1.SelectedDate.ToShortDateString() + '.';
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            //first check to make sure everything is valid
            if (!Page.IsValid)
            {
                return;
            }

           //Creating the customer object

            string name = userName.Text.ToString();
            string email = userEmail.Text.ToString();
            string phone = userPhone.Text.ToString();
            Customer newcustomer = new Customer();
            newcustomer.CustName = name;
            newcustomer.CustPhone = phone;
            newcustomer.CustEmail = email;

            //Creating the booking object
            int NumberNights = int.Parse(nights.Text);
            string NumberAdults = DropDownList2.Text.ToString();
            string NumberKids = DropDownList3.Text.ToString();

            Booking newbooking = new Booking();
            newbooking.NumNights = NumberNights;
            newbooking.NumAdults = NumberAdults;
            newbooking.NumKids = NumberKids;

            //Creating the room object
            List<String> additionalsRoom = new List<String> {" Seaside view and ", "Balcony for "};
            Room newroom = new Room(additionalsRoom);
            
            List<string> roomStyle = new List<string>();

            foreach (Control control in rooms_container.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox room = (CheckBox)control;
                    if (room.Checked)
                    {
                        roomStyle.Add(room.Text);
                    }

                }
            }

            newroom.additionalsRoom = newroom.additionalsRoom.Concat(roomStyle).ToList();
            RoomInfo.InnerHtml = "Room selection including additionals:" + String.Join(" ", newroom.additionalsRoom.ToArray());

            CustInfo.InnerHtml = "The booking is made under the name: " + newcustomer.CustName;
            BookingInfo.InnerHtml = "Booking for " +newbooking.NumAdults + " adults and " + newbooking.NumKids + " kids for " + newbooking.NumNights + " nights";

           
        

        }
    }
}