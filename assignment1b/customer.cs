﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment1b
{
    public class Customer
    {
        //customer info
        private string custName;
        private string custPhone;
        private string custEmail;


        public string CustName
        {
            get
            {
                return custName;
            }
            set
            {
                custName = value;
            }
        }

        public string CustPhone
        {
            get
            {
                return custPhone;
            }
            set
            {
                custPhone = value;
            }
        }

        public string CustEmail
        {
            get
            {
                return custEmail;
            }
            set
            {
                custEmail = value;
            }

        }

       
    }
}

