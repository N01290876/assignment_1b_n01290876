﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1b.aspx.cs" Inherits="assignment1b.WebForm1b" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
   
    <form id="form1" runat="server">
        <div>
            <h2>Book your stay at Paradise Hotel</h2>
        </div>
        <div id="lab" runat="server"></div>
        <label>Full Name:</label><br />
            <asp:TextBox runat="server" ID="userName"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your full name" ControlToValidate="userName" ID="customerName"></asp:RequiredFieldValidator>
            <br /><br />
            <label>Phone Number:</label><br />
            <asp:TextBox runat="server" ID="userPhone" placeholder="xxx-xxx-xxxx"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="userPhone" ID="customerPhone"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ControlToValidate="userPhone" Type="String" Operator="NotEqual" ValueToCompare="1231231234" ErrorMessage="Please enter a valid phone number"></asp:CompareValidator>
            <br /><br />
            <label>Email:</label><br />
            <asp:TextBox runat="server" ID="userEmail" placeholder="eg: abc@gmail.com"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="userEmail" ID="customerEmail"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ControlToValidate="userEmail" ID="regexEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"  ErrorMessage="Please enter a valid Email"></asp:RegularExpressionValidator>
            <br /><br />
            <label>Check in date:</label><br />
            <asp:Calendar ID="Calendar1" runat="server" OnSelectionChanged="DateChange">
            </asp:Calendar>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><br /><br />
            <br />
            <label>Number of nights?</label><br />
            <asp:TextBox runat="server" ID="nights" placeholder="Must be greater than zero"></asp:TextBox>
            <asp:RangeValidator runat="server" ControlToValidate="nights" Type="Integer" MinimumValue="1" MaximumValue="15" ErrorMessage="Enter valid number of nights (greater than zero)"></asp:RangeValidator>
            <br />
            <br /><br />
         <label>Number of Adults?</label><br />
            <asp:DropDownList runat="server" ID="DropDownList2">
                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                <asp:ListItem Value="6" Text="6"></asp:ListItem>
            </asp:DropDownList>
        <br /><br />
         <label>Number of Kids?</label><br />
            <asp:DropDownList runat="server" ID="DropDownList3">
                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                <asp:ListItem Value="6" Text="6"></asp:ListItem>
            </asp:DropDownList><br /><br />
        <label>Type of room:</label><br />
            <div id="rooms_container" runat="server">
                <asp:CheckBox runat="server" ID="roomType1" Text="1 adult room"/>
                <asp:CheckBox runat="server" ID="roomType2" Text="2 adults room"/>
                <asp:CheckBox runat="server" ID="roomType3" Text="2 adults room + 2 kids (2-12 years)"/>
                <asp:CheckBox runat="server" ID="roomType4" Text="Villa for 7"/>
            </div>
           
            <br />

         <br />
        <div>
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Book Now"  />
            <br /><br />
            <div runat="server" id="CustInfo"></div>
            <div runat="server" id="BookingInfo"></div>
            <div runat="server" id="CheckinInfo"></div>
            <div runat="server" id="RoomInfo"></div>

            </div>
        
     
    </form>
   
</body>
</html>
